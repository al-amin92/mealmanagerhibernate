package net.therap.mealmanager.main;

import net.therap.mealmanager.queryprocessor.Input;

/**
 * @author al-amin
 * @since 11/20/16
 */

public class Main {
    public static void main(String[] args) {

        Input input = new Input();
        input.takeInput();
        System.exit(0);
    }
}
