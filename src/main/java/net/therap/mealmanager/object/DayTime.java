package net.therap.mealmanager.object;

import java.util.HashMap;
import java.util.Map;

/**
 * @author al-amin
 * @since 11/21/16
 */

public class DayTime {

    private Map<String, Integer> dayId = new HashMap<>();
    private Map<String, Integer> timeId = new HashMap<>();
    private String day[] = {"", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};
    private String time[] = {"", "BREAKFAST", "LUNCH", "DINNER"};

    public DayTime() {

        dayId.put("SU", 1);
        dayId.put("MO", 2);
        dayId.put("TU", 3);
        dayId.put("WE", 4);
        dayId.put("TH", 5);
        dayId.put("FR", 6);
        dayId.put("SA", 7);
        timeId.put("B", 1);
        timeId.put("L", 2);
        timeId.put("D", 3);
    }

    public String getDayById(int id) {

        if (id >= 1 && id <= 7) {
            return day[id];
        }

        return null;
    }

    public String getTimeById(int id) {

        if (id >= 1 && id <= 3) {
            return time[id];
        }

        return null;
    }

    public int getIdByDay(String day) {

        if (day.length() >= 2 && dayId.containsKey(day.substring(0, 2).toUpperCase())) {
            return dayId.get(day.substring(0, 2).toUpperCase());
        }

        return 0;
    }

    public int getIdByTime(String time) {

        if (time.length() >= 1 && timeId.containsKey(time.substring(0, 1).toUpperCase())) {
            return timeId.get(time.substring(0, 1).toUpperCase());
        }

        return 0;
    }
}
