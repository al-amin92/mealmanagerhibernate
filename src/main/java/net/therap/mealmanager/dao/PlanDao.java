package net.therap.mealmanager.dao;

import net.therap.mealmanager.entity.Item;
import net.therap.mealmanager.entity.Plan;
import net.therap.mealmanager.object.DayTime;
import net.therap.mealmanager.utility.Util;
import net.therap.mealmanager.view.Show;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

/**
 * @author al-amin
 * @since 11/21/16
 */

public class PlanDao {

    private final int day = 7, time = 3;
    DayTime dayTime = new DayTime();

    public PlanDao() {

        for (int i = 1; i <= day; i++) {
            for (int j = 1; j <= time; j++) {
                SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
                Session session = sessionFactory.getCurrentSession();
                Transaction tx = session.beginTransaction();

                Query query = session.createQuery("FROM Plan WHERE day = :day AND time = :time");
                query.setInteger("day", i);
                query.setInteger("time", j);

                if (query.uniqueResult() == null) {
                    Plan plan = new Plan();
                    plan.setDay(i);
                    plan.setTime(j);
                    plan.setCost(0);
                    session.save(plan);
                    session.getTransaction().commit();
                } else {
                    session.close();
                }
            }
        }
    }

    public void query() {

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM Plan");

        List<Plan> mealList = query.list();
        Show.showMeal(mealList);

        tx.commit();
    }

    public void query(String day) {

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM Plan WHERE day= :day");
        query.setInteger("day", dayTime.getIdByDay(day));

        List<Plan> mealList = query.list();
        Show.showMeal(mealList);

        tx.commit();
    }

    public void query(String day, String time) {

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM Plan WHERE day= :day AND time= :time");
        query.setInteger("day", dayTime.getIdByDay(day));
        query.setInteger("time", dayTime.getIdByTime(time));

        List<Plan> mealList = query.list();
        Show.showMeal(mealList);

        tx.commit();
    }

    public String getIdByDayTime(String day, String time) {

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM Plan WHERE day= :day AND time= :time");
        query.setInteger("day", dayTime.getIdByDay(day));
        query.setInteger("time", dayTime.getIdByTime(time));

        Plan plan = (Plan) query.uniqueResult();
        String id = Integer.toString(plan.getId());

        tx.commit();

        return id;
    }

    public void update(String id) {

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM Plan WHERE id= :id");
        query.setInteger("id", Integer.parseInt(id));

        Plan plan = (Plan) query.uniqueResult();
        List<Item> items = plan.getItems();

        int totalCost = 0;
        for (Item item : items) {
            totalCost += item.getCost();
        }
        plan.setCost(totalCost);

        session.update(plan);
        session.save(plan);
        session.getTransaction().commit();
    }

    public void delete(String day, String time, String itemId) {

        String id = getIdByDayTime(day, time);

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM Item WHERE id = :itemId");
        query.setString("itemId", itemId);

        Item item = (Item) query.uniqueResult();

        Query query2 = session.createQuery("FROM Plan WHERE id = :id");
        query2.setString("id", id);

        Plan plan = (Plan) query2.uniqueResult();
        plan.deleteItem(item);

        session.update(plan);
        session.save(plan);
        session.getTransaction().commit();

        update(id);
    }

    public void set(String day, String time, String itemId) {

        String id = getIdByDayTime(day, time);

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM Item WHERE id = :itemId");
        query.setString("itemId", itemId);

        Item item = (Item) query.uniqueResult();

        Query query2 = session.createQuery("FROM Plan WHERE id = :id");
        query2.setString("id", id);

        Plan plan = (Plan) query2.uniqueResult();
        plan.resetItems();
        plan.insertItem(item);

        session.update(plan);
        session.save(plan);
        session.getTransaction().commit();

        update(id);

    }

    public void insert(String day, String time, String itemId) {

        String id = getIdByDayTime(day, time);

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();

        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM Item WHERE id = :itemId");
        query.setString("itemId", itemId);

        Item item = (Item) query.uniqueResult();

        Query query2 = session.createQuery("FROM Plan WHERE id = :id");
        query2.setString("id", id);

        Plan plan = (Plan) query2.uniqueResult();
        plan.insertItem(item);

        session.update(plan);
        session.save(plan);
        session.getTransaction().commit();

        update(id);
    }

    public void deleteMeal(String day, String time) {

        String id = getIdByDayTime(day, time);

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM Plan WHERE id = :id");
        query.setString("id", id);

        Plan plan = (Plan) query.uniqueResult();
        plan.resetItems();

        session.update(plan);
        session.save(plan);
        session.getTransaction().commit();

        update(id);
    }
}
