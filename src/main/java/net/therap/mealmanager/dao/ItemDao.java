package net.therap.mealmanager.dao;

import net.therap.mealmanager.entity.Item;
import net.therap.mealmanager.entity.Plan;
import net.therap.mealmanager.object.DayTime;
import net.therap.mealmanager.utility.Util;
import net.therap.mealmanager.view.Show;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

/**
 * @author al-amin
 * @since 11/21/16
 */

public class ItemDao {

    public void query() {

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM Item");
        List<Item> itemList = query.list();
        Show.showItems(itemList);

        tx.commit();
    }

    public void query(String id) {

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("FROM Item WHERE id = :id");
        query.setString("id", id);
        Item item = (Item) query.uniqueResult();
        List<Plan> plans = item.getPlans();
        DayTime dayTime = new DayTime();

        Show.print("id: " + item.getId() + ", name:" + item.getName() + ", cost:" + item.getCost());

        for (Plan plan : plans) {
            Show.print(dayTime.getDayById(plan.getDay()) + " " + dayTime.getTimeById(plan.getTime()));
        }

        Show.print("");
        tx.commit();
    }

    public void delete(String id) {

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Query query = session.createQuery("DELETE FROM Item WHERE id= :id");
        query.setString("id", id);
        query.executeUpdate();

        tx.commit();
    }

    public void insert(String name, String cost) {

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Item item = new Item();
        item.setName(name);
        item.setCost(Integer.parseInt(cost));
        session.save(item);

        tx.commit();
    }

}
