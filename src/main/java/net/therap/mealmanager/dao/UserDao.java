package net.therap.mealmanager.dao;

import net.therap.mealmanager.entity.User;
import net.therap.mealmanager.utility.Util;
import net.therap.mealmanager.view.Show;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * @author al-amin
 * @since 11/21/16
 */

public class UserDao {

    public static boolean admin = false, loggedIn = false;

    public UserDao() {

        SessionFactory sessionFactory2 = Util.getSessionAnnotationFactory();
        Session session2 = sessionFactory2.getCurrentSession();
        Transaction tx2 = session2.beginTransaction();

        Query query2 = session2.createQuery("FROM User WHERE username LIKE 'admin'");
        if (query2.uniqueResult() == null) {
            User user2 = new User();
            user2.setUsername("admin");
            user2.setPassword("therap");
            user2.setIsAdmin(true);
            session2.save(user2);
            session2.getTransaction().commit();
        } else {
            session2.close();
        }
    }

    public void login(String name, String password) {

        if (loggedIn) {
            Show.print("You are already logged in.");
            return;
        }


        //session2.close();
        //tx2.commit();
        //tx.commit();

        //SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        //Session session = sessionFactory.getCurrentSession();
        //Transaction tx = session.beginTransaction();

        SessionFactory sessionFactory = Util.getSessionAnnotationFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("FROM User WHERE username LIKE :name");
        query.setString("name", name);

        User user = (User) query.uniqueResult();

        if (user.getPassword().equals(password)) {
            loggedIn = true;
            if (user.getIsAdmin()) {
                admin = true;
                Show.print("Logged in as " + name + "(admin)");
            } else {
                Show.print("Logged in as " + name);
            }
        } else {
            admin = loggedIn = false;
            Show.print("username and password doesn't match, try again.");
        }
        tx.commit();
    }

    public void logout() {

        if (!loggedIn) {
            Show.print("You are not logged in.");
        } else {
            Show.print("Logged out.");
            admin = loggedIn = false;
        }
    }

}
