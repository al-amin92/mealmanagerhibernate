package net.therap.mealmanager.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author al-amin
 * @since 11/20/16
 */

@Entity
@Table(name = "plan", uniqueConstraints = {@UniqueConstraint(columnNames = {"id"})})
public class Plan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true, length = 11)
    private int id;

    @Column(name = "day", nullable = false)
    private int day;

    @Column(name = "time", nullable = false)
    private int time;

    @Column(name = "cost", nullable = false)
    private int cost;

    public Plan() {

    }

    public Plan(int day, int time, int cost) {
        this.day = day;
        this.time = time;
        this.cost = cost;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "meal", joinColumns = {@JoinColumn(name = "plan_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "item_id", referencedColumnName = "id")})
    private List<Item> items = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void deleteItem(Item item) {
        this.items.remove(item);
    }

    public void insertItem(Item item) {
        this.items.add(item);
    }

    public void resetItems() {
        this.items.clear();
    }
}
