package net.therap.mealmanager.queryprocessor;

import net.therap.mealmanager.dao.ItemDao;
import net.therap.mealmanager.dao.PlanDao;
import net.therap.mealmanager.dao.UserDao;
import net.therap.mealmanager.view.Show;

/**
 * @author al-amin
 * @since 11/21/16
 */

public class QueryProcessor {

    boolean process(String command) {

        ItemDao itemDao = new ItemDao();
        PlanDao planDao = new PlanDao();
        UserDao userDao = new UserDao();

        if (command.equals("exit")) {
            return false;
        }

        String cmd[] = command.split(" ");
        int len = cmd.length;

        if (len == 1 && cmd[0].equals("help")) {
            Show.showHelp(UserDao.admin);
        } else if (len >= 2 && cmd[0].equals("show")) {
            if (len == 2 && cmd[1].equals("item")) {
                itemDao.query();
            } else if (len == 3 && cmd[1].equals("item")) {
                itemDao.query(cmd[2]);
            } else if (len >= 2 && cmd[1].equals("meal")) {
                if (len == 2) {
                    planDao.query();
                } else if (len == 3) {
                    planDao.query(cmd[2]);
                } else if (len == 4) {
                    planDao.query(cmd[2], cmd[3]);
                }
            }
        } else if (len >= 5 && cmd[0].equals("set")) {
            if (!userDao.admin) {
                Show.print("Access Denied! Please log in as an admin first.\n");
                return true;
            }
            for (int i = 4; i < len; i++) {
                if (i == 4) {
                    planDao.set(cmd[2], cmd[3], cmd[i]);
                } else {
                    planDao.insert(cmd[2], cmd[3], cmd[i]);
                }
            }
        } else if (len >= 3 && cmd[0].equals("delete")) {
            if (!userDao.admin) {
                Show.print("Access Denied! Please log in as an admin first.\n");
                return true;
            }
            if (len >= 3 && cmd[1].equals("item")) {
                for (int i = 2; i < cmd.length; i++) {
                    itemDao.delete(cmd[i]);
                }
            } else if (len == 4 && cmd[1].equals("meal")) {
                planDao.deleteMeal(cmd[2], cmd[3]);
            } else if (len >= 5 && cmd[1].equals("meal")) {
                for (int i = 4; i < cmd.length; i++) {
                    planDao.delete(cmd[2], cmd[3], cmd[i]);
                }
            }
        } else if (len == 3 && cmd[0].equals("login")) {
            userDao.login(cmd[1], cmd[2]);
        } else if (len == 1 && cmd[0].equals("logout")) {
            userDao.logout();
        } else if (cmd[0].equals("insert")) {
            if (!userDao.admin) {
                Show.print("Access Denied! Please log in as an admin first.\n");
                return true;
            }
            if (len == 4 && cmd[1].equals("item")) {
                itemDao.insert(cmd[2], cmd[3]);
            } else if (len >= 5 && cmd[1].equals("meal")) {
                for (int i = 4; i < len; i++) {
                    planDao.insert(cmd[2], cmd[3], cmd[i]);
                }

            }
        }

        return true;
    }
}
